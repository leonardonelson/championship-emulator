package com.esports.championship.emulator;

import com.esports.championship.emulator.results.JdbcResultsRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public JdbcResultsRepository jdbcResultsRepository(DataSource dataSource) {
        return new JdbcResultsRepository(dataSource);
    }
}
