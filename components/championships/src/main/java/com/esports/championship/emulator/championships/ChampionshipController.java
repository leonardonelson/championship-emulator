package com.esports.championship.emulator.championships;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChampionshipController {

    private static final Logger log = LoggerFactory.getLogger(ChampionshipController.class.getName());

    private ChampionshipRepository championshipRepository;

    public ChampionshipController(ChampionshipRepository championshipRepository) {
        this.championshipRepository = championshipRepository;
    }

    @PostMapping("/championship")
    public ResponseEntity<Championship> createChampionship(@RequestBody Championship championship){
        log.info("Adding a new championship: {}", championship.getName());
        championship = championshipRepository.save(championship);
        return new ResponseEntity<>(championship, HttpStatus.OK);
    }
}
