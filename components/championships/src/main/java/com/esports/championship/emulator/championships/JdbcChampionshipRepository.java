package com.esports.championship.emulator.championships;

import com.esports.championship.emulator.championships.Championship;
import com.esports.championship.emulator.championships.ChampionshipRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

@Repository
public class JdbcChampionshipRepository implements ChampionshipRepository {

    private final String INSERT_CHAMPIONSHIP_SQL = "INSERT INTO championship (name) VALUES (?)";
    private final String FIND_CHAMPIONSHIP_SQL = "SELECT * FROM championship WHERE id = ?";
    private JdbcTemplate jdbcTemplate;
    private final RowMapper<Championship> championshipRowMapper;
    private final ResultSetExtractor<Championship> extractor;

    public JdbcChampionshipRepository(DataSource datasource) {
        this.jdbcTemplate = new JdbcTemplate(datasource);

        championshipRowMapper = (rs, rowNum) ->
                new Championship(rs.getLong("id"), rs.getString("name"));

        extractor = (rs) -> rs.next() ? championshipRowMapper.mapRow(rs, 1) : null;
    }

    @Override
    public Championship save(Championship championship) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_CHAMPIONSHIP_SQL, RETURN_GENERATED_KEYS);
            ps.setString(1, championship.getName());
            return ps;
        }, keyHolder);

        return find(keyHolder.getKey().longValue());
    }

    @Override
    public Championship find(long id) {
        return jdbcTemplate.query(
                FIND_CHAMPIONSHIP_SQL,
                new Object[]{id},
                extractor
        );
    }
}
