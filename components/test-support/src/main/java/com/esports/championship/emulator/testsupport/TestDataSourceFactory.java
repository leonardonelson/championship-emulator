package com.esports.championship.emulator.testsupport;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;

public class TestDataSourceFactory {

    public static DataSource create(String name) {
        MysqlDataSource dataSource = new MysqlDataSource();

        dataSource.setUrl("jdbc:mysql://192.168.99.100:3306/" + name + "?useSSL=false&useTimezone=true&serverTimezone=UTC&useLegacyDatetimeCode=false");
        dataSource.setUser("root");
        dataSource.setPassword("my-secret-pw");

        return dataSource;
    }
}
