package com.esports.championship.emulator.results;

import com.esports.championship.emulator.teams.TeamStats;

import java.util.ArrayList;
import java.util.List;

public class ResultSummary {
    private String groupName;
    private long groupId;
    private List<TeamStats> teamsStats = new ArrayList<>();
    private List<Result> results = new ArrayList<>();

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public List<TeamStats> getTeamsStats() {
        return teamsStats;
    }

    public void setTeamsStats(List<TeamStats> teamsStats) {
        this.teamsStats = teamsStats;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
