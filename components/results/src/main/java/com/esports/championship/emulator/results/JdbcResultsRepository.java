package com.esports.championship.emulator.results;

import com.esports.championship.emulator.teams.TeamStats;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

@Repository
public class JdbcResultsRepository implements ResultsRepository {

    private final String INSERT_RESULT_SQL = "INSERT INTO result (win_team, team1, team2, group_id, score1, score2)" +
            " VALUES (?, ?, ?, ?, ?, ?)";
    private final String FIND_RESULT_SQL = "SELECT * FROM result WHERE id = ?";
    private final String FIND_RESULTS_BY_GROUP_SQL = "SELECT * FROM result WHERE group_id = ?;";
    private final String FIND_TEAM_STATS_SQL = "select t.name, t.id, (select count(win_team) from result r where r.win_team = t.id) as wins," +
            " (select cast(coalesce(sum(score1), 0) as signed) from result where win_team = t.id) +" +
            " (select cast(coalesce(sum(score2), 0) as signed) from result where (win_team != t.id and (team1 = t.id or team2 = t.id))) - " +
            " (select cast(coalesce(sum(score1), 0) as signed) from result where (win_team != t.id and (team1 = t.id or team2 = t.id))) as round_balance" +
            " from team t" +
            " inner join group_team gt on gt.team_id = t.id" +
            " where gt.group_id = ?" +
            " order by wins desc, round_balance desc;";

    private JdbcTemplate jdbcTemplate;
    private final RowMapper<Result> resultRowMapper;
    private final ResultSetExtractor<Result> extractor;

    public JdbcResultsRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);

        resultRowMapper = (rs, rowNum) ->
                new Result(rs.getLong("id"),
                        rs.getLong("win_team"),
                        rs.getLong("team1"),
                        rs.getLong("team2"),
                        rs.getLong("group_id"),
                        rs.getInt("score1"),
                        rs.getInt("score2"));

        extractor = (rs) -> rs.next() ? resultRowMapper.mapRow(rs, 1) : null;
    }

    @Override
    public Result save(Result result) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_RESULT_SQL, RETURN_GENERATED_KEYS);
            ps.setLong(1, result.getWinTeam());
            ps.setLong(2, result.getTeam1());
            ps.setLong(3, result.getTeam2());
            ps.setLong(4, result.getGroupId());
            ps.setInt(5, result.getScore1());
            ps.setInt(6, result.getScore2());
            return ps;
        }, keyHolder);

        return find(keyHolder.getKey().longValue());
    }

    @Override
    public Result find(long id) {
        return jdbcTemplate.query(
                FIND_RESULT_SQL,
                new Object[]{id},
                extractor
        );
    }

    @Override
    public List<Result> findResultsByGroup(long groupId) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                FIND_RESULTS_BY_GROUP_SQL,
                new Object[]{groupId}
        );

        List<Result> results = new ArrayList<>();

        for (Map row : rows) {
            Result result = new Result();
            result.setId((Long) row.get("id"));
            result.setGroupId((Long) row.get("group_id"));
            result.setWinTeam((Long) row.get("win_team"));
            result.setTeam1((Long) row.get("team1"));
            result.setTeam2((Long) row.get("team2"));
            result.setScore1((Integer) row.get("score1"));
            result.setScore2((Integer) row.get("score2"));
            results.add(result);
        }
        return results;
    }

    @Override
    public List<TeamStats> findTeamsStatsByGroup(long groupId) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                FIND_TEAM_STATS_SQL,
                new Object[]{groupId}
        );

        List<TeamStats> teamsStats = new ArrayList<>();

        for (Map row : rows) {
            teamsStats.add(new TeamStats((Long) row.get("id"),
                    (String) row.get("name"),
                    (Long) row.get("wins"),
                    (Long) row.get("wins"),
                    (Long) row.get("round_balance")));
        }

        return teamsStats;
    }

}
