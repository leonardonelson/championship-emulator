package com.esports.championship.emulator.results;

public class Result {
    private long id;
    private long winTeam;
    private long team1;
    private long team2;
    private long groupId;
    private int score1;
    private int score2;

    public Result(long id, long winTeam, long team1, long team2, long groupId, int score1, int score2) {
        this.id = id;
        this.winTeam = winTeam;
        this.team1 = team1;
        this.team2 = team2;
        this.groupId = groupId;
        this.score1 = score1;
        this.score2 = score2;
    }

    public Result() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getWinTeam() {
        return winTeam;
    }

    public void setWinTeam(long winTeam) {
        this.winTeam = winTeam;
    }

    public long getTeam1() {
        return team1;
    }

    public void setTeam1(long team1) {
        this.team1 = team1;
    }

    public long getTeam2() {
        return team2;
    }

    public void setTeam2(long team2) {
        this.team2 = team2;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public int getScore1() {
        return score1;
    }

    public void setScore1(int score1) {
        this.score1 = score1;
    }

    public int getScore2() {
        return score2;
    }

    public void setScore2(int score2) {
        this.score2 = score2;
    }
}
