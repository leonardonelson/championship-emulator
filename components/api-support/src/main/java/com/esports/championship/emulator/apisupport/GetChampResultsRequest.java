package com.esports.championship.emulator.apisupport;

public class GetChampResultsRequest {
    private Long champId;
    private Integer round;

    public Long getChampId() {
        return champId;
    }

    public void setChampId(Long champId) {
        this.champId = champId;
    }

    public Integer getRound() {
        return round;
    }

    public void setRound(Integer round) {
        this.round = round;
    }
}
