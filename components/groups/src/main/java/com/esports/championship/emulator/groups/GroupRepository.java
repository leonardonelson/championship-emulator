package com.esports.championship.emulator.groups;

import com.esports.championship.emulator.apisupport.AddTeamToGroupRequest;
import com.esports.championship.emulator.persistencesupport.BaseRepository;

import java.util.List;

public interface GroupRepository extends BaseRepository<Group> {
    boolean addTeamToGroup(AddTeamToGroupRequest addTeamToGroupRequest);

    List<Group> findAllByChampIdAndRound(Long champId, Integer round);
}
