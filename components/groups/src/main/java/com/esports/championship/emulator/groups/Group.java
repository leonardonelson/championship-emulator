package com.esports.championship.emulator.groups;

public class Group {
    private long id;
    private long champId;
    private String name;
    private Round round;

    public Group(long id, long champId, String name, Round round) {
        this.id = id;
        this.champId = champId;
        this.name = name;
        this.round = round;
    }

    public Group() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getChampId() {
        return champId;
    }

    public void setChampId(long champId) {
        this.champId = champId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }
}
