package com.esports.championship.emulator.teams;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

@Repository
public class JdbcTeamRepository implements TeamRepository {

    private final String INSERT_TEAM_SQL = "INSERT INTO team (name) VALUES (?)";
    private final String FIND_TEAM_SQL = "SELECT * FROM team WHERE id = ?";
    private JdbcTemplate jdbcTemplate;
    private final RowMapper<Team> teamRowMapper;
    private final ResultSetExtractor<Team> extractor;

    public JdbcTeamRepository(DataSource datasource) {
        this.jdbcTemplate = new JdbcTemplate(datasource);

        teamRowMapper = (rs, rowNum) ->
                new Team(rs.getLong("id"), rs.getString("name"));

        extractor = (rs) -> rs.next() ? teamRowMapper.mapRow(rs, 1) : null;
    }

    @Override
    public Team save(Team team) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_TEAM_SQL, RETURN_GENERATED_KEYS);
            ps.setString(1, team.getName());
            return ps;
        }, keyHolder);

        return find(keyHolder.getKey().longValue());
    }

    @Override
    public Team find(long id) {
        return jdbcTemplate.query(
                FIND_TEAM_SQL,
                new Object[]{id},
                extractor
        );
    }
}
