package com.esports.championship.emulator.teams;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeamController {

    private static final Logger log = LoggerFactory.getLogger(TeamController.class.getName());

    private TeamRepository teamRepository;

    public TeamController(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @PostMapping("/team")
    public ResponseEntity<Team> createTeam(@RequestBody Team team) {
        log.info("Creating a new Team: {}", team.getName());
        team = teamRepository.save(team);
        return new ResponseEntity<>(team, HttpStatus.OK);
    }
}
