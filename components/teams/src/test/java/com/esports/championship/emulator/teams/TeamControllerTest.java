package com.esports.championship.emulator.api;

import com.esports.championship.emulator.teams.Team;
import com.esports.championship.emulator.teams.TeamController;
import com.esports.championship.emulator.teams.TeamRepository;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.mockito.Mockito.*;

public class TeamControllerTest {

    TeamRepository teamRepository = mock(TeamRepository.class);
    TeamController teamController = new TeamController(teamRepository);

    @Test
    public void testCreateTeam() {
        // Given
        final String teamName = "Mock Sports";
        final Team mockTeam = new Team(1L, teamName);
        final Team team = new Team();
        team.setName(teamName);
        doReturn(mockTeam).when(teamRepository).save(team);
        // When
        ResponseEntity<Team> response = teamController.createTeam(team);
        // Then
        verify(teamRepository).save(team);
        Team teamResponse = response.getBody();
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(teamResponse);
        Assert.assertNotNull(teamResponse.getId());
        Assert.assertEquals(mockTeam.getName(), teamResponse.getName());
        Assert.assertEquals(mockTeam.getId(), teamResponse.getId());
    }
}
